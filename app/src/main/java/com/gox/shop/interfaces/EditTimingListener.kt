package com.gox.shop.interfaces

interface EditTimingListener {
    fun onItemChange(index: Int, isOpenTiming:Boolean)
}