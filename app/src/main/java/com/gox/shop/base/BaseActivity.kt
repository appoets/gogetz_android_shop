package com.gox.shop.base

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.gox.shop.R
import com.gox.shop.application.AppController
import com.gox.shop.dependencies.component.AppComponent
import com.gox.shop.utils.CommanMethods
import com.gox.shop.utils.CustomLoaderDialog
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import es.dmoral.toasty.Toasty
import javax.inject.Inject

abstract  class BaseActivity<T:ViewDataBinding>:AppCompatActivity(){


    protected var baseLiveDataLoading = MutableLiveData<Boolean>()
    private var mViewDataBinding: T? = null
    protected abstract fun initView(mViewDataBinding: ViewDataBinding?)
    private lateinit var mCustomLoaderDialog: CustomLoaderDialog



    @LayoutRes
    protected abstract fun getLayoutId(): Int

    val loadingObservable: MutableLiveData<Boolean> get() = baseLiveDataLoading

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        initView(mViewDataBinding)
        mCustomLoaderDialog = CustomLoaderDialog(this, true)
        baseLiveDataLoading.observe(this, Observer {
            if(it) showLoading()
            else   hideLoading()
        })
    }

    private fun showLoading() {
        try {
            if (mCustomLoaderDialog.window != null) {
                mCustomLoaderDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                mCustomLoaderDialog.show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showErrorToast(msg:String){
//        Toasty.error(this,msg,3000,false).show()
        showAlertWithOK(this,msg)

    }

    fun showSuccessToast(msg:String){
//        Toasty.success(this,msg,3000,false).show()
        showAlertWithOK(this,msg)
    }

    private fun hideLoading() = mCustomLoaderDialog.cancel()


    fun showAlertWithOK(context: Context, messageResId: String) {
        AlertDialog.Builder(context)
            .setTitle(R.string.app_name)
            .setMessage(messageResId)
            .setPositiveButton(android.R.string.yes) { dialog: DialogInterface, which -> Int
                dialog.dismiss()
            }
            .show()
    }
}