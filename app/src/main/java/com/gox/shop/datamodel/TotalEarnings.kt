package com.gox.shop.datamodel

data class TotalEarnings(
    var total_amount: String = ""
)