package com.gox.shop.datamodel

data class ShopTypeModel(
    val error: List<Any> = listOf(),
    val message: String = "",
    val responseData: ArrayList<ResponseData> = arrayListOf(),
    val statusCode: String = "",
    val title: String = ""
) {
    data class ResponseData(
        val category: String = "",
        val id: Int = 0,
        val name: String = "",
        val status: Int = 0
    )
}