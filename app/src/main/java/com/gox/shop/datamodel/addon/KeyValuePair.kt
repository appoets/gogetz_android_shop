package com.gox.shop.datamodel.addon

class KeyValuePair(var key: String, var value: String) {
    override fun toString(): String {
        return value
    }
}