package com.gox.shop.views.product.category

interface CategoriesNavigator {
    fun openCreateCategoryActivity()
    fun openEditActivity(id:Int)
}