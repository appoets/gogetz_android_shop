package com.gox.shop.views.product.productDML

import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.datamodel.product.ProductItemsModel
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import javax.inject.Inject

class ProductListViewModel : BaseViewModel<ProductListNavigator>() {
    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()
    var productListItems = MutableLiveData<ProductItemsModel>()
    var loadingLiveData = MutableLiveData<Boolean>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository

    init {
        AppController.appComponent.inject(this)
    }


    fun apiCallProducts(hashMap: HashMap<String, Any>, page : Int) {
        /* hashMap.put(
             "store_id", sessionManager.sharedPreferences.getInt(
                 PreferenceKey.SHOP_ID,
                 0
             )
         )*/
        loadingLiveData.value = true
        getCompositeDisposable().add(
            shopRepository.getProductList(
                object : ApiListener {
                    override fun success(successData: Any) {
                        //  commonSuccessResponse.value = successData as CommonSuccessResponse
                        productListItems.value = successData as ProductItemsModel
                    }

                    override fun fail(failData: Throwable) {
                        loadingLiveData.value = false
                    }

                }, sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                ), "10", "0",page.toString()
            )
        )
    }

    fun deleteProduct(id: Int) {
        loadingLiveData.value = true

        getCompositeDisposable().add(
            shopRepository.deletProduct(
                object : ApiListener {
                    override fun success(successData: Any) {
                        loadingLiveData.value = false
                         commonSuccessResponse.value = successData as CommonSuccessResponse
                    }

                    override fun fail(failData: Throwable) {
                        loadingLiveData.value = false
                    }

                }, id)
        )
    }
    fun openEditProductActivity(id:Int){
        navigator.openEditProductActivity(id)
    }

    fun openCreateProductActivity(){
        navigator.openCreateProductActivity()
    }
}