package com.gox.shop.views.product.addons.create

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import javax.inject.Inject

class AddAddOnViewModel : BaseViewModel<AddAddOnNavigator>() {

    var addOnName = ObservableField<String>("")
    var addOnStatus = ObservableField<Boolean>(false)
    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository

    init {
        AppController.appComponent.inject(this)
    }

    fun createAddOn() {
        navigator.createAddFunction()
    }

    fun apiCallAddaddOn(hashMap: HashMap<String, Any>) {
        hashMap.put(
            "store_id", sessionManager.sharedPreferences.getInt(
                PreferenceKey.SHOP_ID,
                0
            )
        )
        getCompositeDisposable().add(shopRepository.addAddOn(object : ApiListener {
            override fun success(successData: Any) {
                commonSuccessResponse.value = successData as CommonSuccessResponse
            }

            override fun fail(failData: Throwable) {

            }

        }, hashMap))
    }
}