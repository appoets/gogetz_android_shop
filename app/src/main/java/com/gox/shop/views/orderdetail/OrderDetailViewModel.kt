package com.gox.shop.views.orderdetail

import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.datamodel.HistoryDetailModel
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import es.dmoral.toasty.Toasty
import javax.inject.Inject

class OrderDetailViewModel : BaseViewModel<OrderDetailNavigator>() {
    var orderDetailResponse = MutableLiveData<HistoryDetailModel>()
    var takeAwayResponse = MutableLiveData<CommonSuccessResponse>()
    var mLoadingProgress = MutableLiveData<Boolean>()
    var orderID = MutableLiveData<Int>()


    init {
        AppController.appComponent.inject(this)
    }

    @Inject
    lateinit var shopRepository: ShopRepository

    fun callOrderDetail() {
        mLoadingProgress.value = true
        if (orderID.value != null)
            shopRepository.getOrderDetail(orderID.value!!, object : ApiListener {
                override fun success(successData: Any) {
                    mLoadingProgress.value = false
                    orderDetailResponse.postValue(successData as HistoryDetailModel)
                }

                override fun fail(failData: Throwable) {
                    mLoadingProgress.value = false
                }

            })
    }

    fun callTakeAway(id: String, store_id: String, status: String) {
        if (orderID.value != null)
            shopRepository.takeAway(id, store_id, status, object : ApiListener {
                override fun success(successData: Any) {
                    mLoadingProgress.value = false
                    takeAwayResponse.postValue(successData as CommonSuccessResponse)
                }

                override fun fail(failData: Throwable) {
                    mLoadingProgress.value = false
                }

            })
    }
}