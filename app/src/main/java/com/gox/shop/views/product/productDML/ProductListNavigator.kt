package com.gox.shop.views.product.productDML

interface ProductListNavigator {
    fun openCreateProductActivity()
    fun openEditProductActivity(id:Int)
}