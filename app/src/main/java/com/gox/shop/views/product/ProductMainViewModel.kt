package com.gox.shop.views.product

import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.dependencies.component.AppComponent

class ProductMainViewModel : BaseViewModel<ProductMainNavigator>() {

    var mainViewModel = ArrayList<ProductMainModel.ProductMain>()

    init {
        AppController.appComponent.inject(this)
    }


}

data class ProductMainModel(var items: List<ProductMain>) {
    data class ProductMain(var id: Int, var menuName: String, var imageViewDrawable: Int)
}