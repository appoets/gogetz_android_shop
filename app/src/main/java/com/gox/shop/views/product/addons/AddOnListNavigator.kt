package com.gox.shop.views.product.addons

interface AddOnListNavigator {
    fun openAddOnCreateActivity()
    fun openEditAddOnActivity(addOnId:Int)
}