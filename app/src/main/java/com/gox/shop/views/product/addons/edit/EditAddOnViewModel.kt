package com.gox.shop.views.product.addons.edit

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.datamodel.addon.AddOnDataModel
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import javax.inject.Inject

class EditAddOnViewModel : BaseViewModel<EditAddOnNavigator>() {
    var addOnName = ObservableField<String>("")
    var addOnStatus = ObservableField<Boolean>(false)
    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()
    var addOnResonse = MutableLiveData<AddOnDataModel>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository

    init {
        AppController.appComponent.inject(this)

    }

    fun getSingleAddOnItem(id: Int) {
        getCompositeDisposable().add(shopRepository.getSingelAddOnItem(object : ApiListener {
            override fun success(successData: Any) {
                addOnResonse.value = successData as AddOnDataModel
            }

            override fun fail(failData: Throwable) {

            }

        }, id))
    }

    fun editAddOn() {
        navigator.EditAddOnFunction()
    }

    fun apiCallAddaddOn(addOnId:Int,hashMap: HashMap<String, Any>) {
        hashMap.put(
            "shop_id", sessionManager.sharedPreferences.getInt(
                PreferenceKey.SHOP_ID,
                0
            )
        )
        getCompositeDisposable().add(shopRepository.editAddOn(object : ApiListener {
            override fun success(successData: Any) {
                commonSuccessResponse.value= successData as CommonSuccessResponse
            }

            override fun fail(failData: Throwable) {

            }

        }, addOnId,hashMap))
    }
}