package com.gox.shop.views.product.category.create

import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import es.dmoral.toasty.Toasty
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart
import javax.inject.Inject

class CreateCategoryViewModel : BaseViewModel<CreateCategroyNavigator>() {
    var categoryName = ObservableField<String>()
    var category_desc = ObservableField<String>()

    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()
    var errorResponse = MutableLiveData<String>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository

    init {
        AppController.appComponent.inject(this)
    }


    fun createCategory() {
        navigator.createCategoryMethod()
    }

    fun imageUpload() {
        navigator.selectImage()
    }

    fun createCategoryApi(params: HashMap<String, RequestBody>, file: MultipartBody.Part) {
        params.put(
            "store_id",
            RequestBody.create(
                MediaType.parse("text/plain"),
                "" + sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                )
            )
        )
        getCompositeDisposable().add(
            shopRepository.createCategory(
                params,
                file,
                object : ApiListener {
                    override fun success(successData: Any) {
                        commonSuccessResponse.value = successData as CommonSuccessResponse
                    }

                    override fun fail(failData: Throwable) {
                        errorResponse.value = "" + failData.message.toString()
                    }

                })
        )
    }
}