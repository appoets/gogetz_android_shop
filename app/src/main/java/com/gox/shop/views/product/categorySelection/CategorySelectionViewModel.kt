package com.gox.shop.views.product.categorySelection

import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonErrorResponse
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.datamodel.category.CategoryItemsModel
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import javax.inject.Inject

class CategorySelectionViewModel : BaseViewModel<CategorySelectionNavigator>() {
    var categoryItemsModel = MutableLiveData<CategoryItemsModel>()
    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()
    var categoryModel=MutableLiveData<CategoryItemsModel.Category>()
    var errorResponse = MutableLiveData<CommonErrorResponse>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository


    init {
        AppController.appComponent.inject(this)
        getCategoryList()
    }

    fun getCategoryList() {
        getCompositeDisposable().add(
            shopRepository.getCategoryList(
                sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                ), "10", "0", object : ApiListener {
                    override fun success(successData: Any) {
                        categoryItemsModel.value = successData as CategoryItemsModel
                    }

                    override fun fail(failData: Throwable) {

                    }

                })
        )
    }

}