package com.gox.shop.views.countrylist

import com.gox.shop.base.BaseViewModel
import com.gox.shop.views.countrylist.CountryNavigator

class CountryViewModel : BaseViewModel<CountryNavigator>() {
    fun closeActivity() {
        navigator.closeActivity()
    }
}