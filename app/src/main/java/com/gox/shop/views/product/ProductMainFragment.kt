package com.gox.shop.views.product

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.gox.shop.R
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseFragment
import com.gox.shop.databinding.FragmentProductMainBinding
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import com.gox.shop.views.adapters.ProductMainAdapter
import com.gox.shop.views.dashboard.DashboardNavigator
import es.dmoral.toasty.Toasty
import javax.inject.Inject

class ProductMainFragment : BaseFragment<FragmentProductMainBinding>(), ProductMainNavigator {

    private lateinit var viewModel: ProductMainViewModel
    private lateinit var productMainBinding: FragmentProductMainBinding
    private lateinit var dashBoardNavigator: DashboardNavigator

    @Inject
    lateinit var sessionManager: SessionManager

    init {
        AppController.appComponent.inject(this)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        dashBoardNavigator = context as DashboardNavigator
        dashBoardNavigator.hideRightIcon(true)
    }

    override fun getLayoutId() = R.layout.fragment_product_main

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        productMainBinding = mViewDataBinding as FragmentProductMainBinding
        viewModel = ViewModelProviders.of(this).get(ProductMainViewModel::class.java)
        productMainBinding.productMainViewModel = viewModel
        viewModel.navigator = this

        viewModel.mainViewModel.clear()

        if (sessionManager.sharedPreferences.getString(PreferenceKey.SHOP_TYPE_NAME,"").equals("FOOD",true)) {
            viewModel.mainViewModel.add(ProductMainModel.ProductMain(1, "ADDONS", R.drawable.image_1))
        }
        viewModel.mainViewModel.add(ProductMainModel.ProductMain(2, "CATEGORY", R.drawable.image_2))
        viewModel.mainViewModel.add(ProductMainModel.ProductMain(3, "PRODUCT", R.drawable.image_3))

        productMainBinding.productMainAdapter =
            ProductMainAdapter(context!!, viewModel.mainViewModel)

        dashBoardNavigator.setTitle(resources.getString(R.string.title_product))
    }


    override fun setProductMainAdapter() {

    }
}