package com.gox.shop.views.product.addons.create

interface AddAddOnNavigator {
    fun createAddFunction()
}