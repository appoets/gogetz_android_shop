package com.gox.shop.views.product.category.edit

interface EditCategoryNavigator {
    fun selectImage()
    fun updateCategoryMethod()
}