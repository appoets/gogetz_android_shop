package com.gox.shop.views.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.gox.shop.R
import com.gox.shop.datamodel.ResponseData
import com.gox.shop.datamodel.ShopTypeModel
import kotlinx.android.synthetic.main.row_shop_type.*
import kotlinx.android.synthetic.main.row_shop_type.view.*

class ShopTypeAdapter(ctx: Context, shopTypeList: ArrayList<ShopTypeModel.ResponseData>):
    ArrayAdapter<ShopTypeModel.ResponseData>(ctx, 0, shopTypeList) {

    private  var shoptTypesList:ArrayList<ShopTypeModel.ResponseData>?=null
    private  var cxt:Context?=null

    init {
        this.shoptTypesList=shopTypeList
        this.cxt=ctx
    }

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val mood = getItem(position)
        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.row_shop_type,
            parent,
            false
        )
        view.tvShopType.text = shoptTypesList!!.get(position).name
        return view
    }
}