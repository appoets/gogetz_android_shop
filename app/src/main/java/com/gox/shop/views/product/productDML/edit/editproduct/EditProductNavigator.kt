package com.gox.shop.views.product.productDML.edit.editproduct

interface EditProductNavigator {

    fun validateInputs()
    fun showError(errorMsg:String)
}