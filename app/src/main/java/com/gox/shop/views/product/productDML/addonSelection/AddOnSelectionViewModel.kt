package com.gox.shop.views.product.productDML.addonSelection

import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.datamodel.addon.AddOnResponseModel
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import javax.inject.Inject

class AddOnSelectionViewModel : BaseViewModel<AddOnSelectionNavigator>() {

    var addOnResponse = MutableLiveData<AddOnResponseModel>()
    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()
    var addOnIdArray= ArrayList<String>()
    var addOnPrice= ArrayList<String>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository

    init {
        AppController.appComponent.inject(this)
        addOnList()
    }

    fun addOnList() {
        getCompositeDisposable().add(
            shopRepository.getAddonList(
                sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                ), object : ApiListener {
                    override fun success(successData: Any) {
                        addOnResponse.value = successData as AddOnResponseModel
                    }

                    override fun fail(failData: Throwable) {

                    }

                })
        )
    }
}