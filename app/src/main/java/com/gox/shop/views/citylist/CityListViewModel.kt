package com.gox.shop.views.citylist

import com.gox.shop.base.BaseViewModel

class CityListViewModel : BaseViewModel<CityListNavigator>() {
    fun closeActivity() {
        navigator.closeActivity()
    }
}