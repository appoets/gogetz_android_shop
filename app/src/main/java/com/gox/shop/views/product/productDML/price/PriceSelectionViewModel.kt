package com.gox.shop.views.product.productDML.price

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.addon.KeyValuePair
import java.util.*

class PriceSelectionViewModel : BaseViewModel<PriceSelectionNavigator>() {
    var price = ObservableField<String>("")
    var discount = ObservableField<String>("")
    var percentage=ObservableArrayList<KeyValuePair>()

}