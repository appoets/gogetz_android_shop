package com.gox.shop.views.forgotPasswordActivity

interface ForgotPasswordNavigator {
    fun showError(error:String)
}
