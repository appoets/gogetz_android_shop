package com.gox.shop.views.product.productDML.edit.editproduct

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.gox.shop.application.AppController
import com.gox.shop.base.BaseViewModel
import com.gox.shop.datamodel.CommonSuccessResponse
import com.gox.shop.datamodel.UnitsData
import com.gox.shop.datamodel.addon.KeyValuePair
import com.gox.shop.datamodel.category.CategoryItemsModel
import com.gox.shop.datamodel.product.CuisineModel
import com.gox.shop.datamodel.product.EditProductModel
import com.gox.shop.dependencies.ApiCallServices.ApiService
import com.gox.shop.interfaces.ApiListener
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.PreferenceKey
import com.gox.shop.utils.SessionManager
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Part
import javax.inject.Inject

class EditProductViewModel : BaseViewModel<EditProductNavigator>() {


    var categoryItemsModel = MutableLiveData<CategoryItemsModel>()
    var cuisineModel = MutableLiveData<CuisineModel>()
    var commonSuccessResponse = MutableLiveData<CommonSuccessResponse>()
    var unitsModel = MutableLiveData<UnitsData>()


    var productName = ObservableField<String>()
    var descriptionString = ObservableField<String>()
    var productCuisine = ObservableField<String>()
    var productOrder = ObservableField<String>()
    var categroyString = ObservableField<String>()
    var cuisuieName = ObservableField<String>()

    var price = ObservableField<String>("")
    var discount = ObservableField<String>("")
    var percentage = ObservableArrayList<KeyValuePair>()

    var addOnIdArray = ArrayList<String>()
    var addOnPrice = ArrayList<String>()
    var editProductReponse= MutableLiveData<EditProductModel>()
    var quantity=ObservableField<String>()
    var selectedUnit = "0"
    var product_Id=""

    var cuisineName = ""
    var cuisineIds = ArrayList<Int>()

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var shopRepository: ShopRepository

    init {
        AppController.appComponent.inject(this)
    }

    init {
        AppController.appComponent.inject(this)
        getCategoryList()
       // getCuisineList()
    }


    fun getCategoryList() {
        getCompositeDisposable().add(
            shopRepository.getCategoryList(
                sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                ), "100", "0", object : ApiListener {
                    override fun success(successData: Any) {
                        categoryItemsModel.value = successData as CategoryItemsModel
                    }

                    override fun fail(failData: Throwable) {

                    }

                })
        )
    }

    fun getUnits() {
        getCompositeDisposable().add(shopRepository.getUnits(sessionManager.sharedPreferences.getInt(PreferenceKey.SHOP_ID, 0), object : ApiListener {
            override fun success(successData: Any) {
                unitsModel.value = successData as UnitsData
            }

            override fun fail(failData: Throwable) {

            }

        })
        )
    }

    fun getCuisineList() {
        getCompositeDisposable().add(
            shopRepository.getCuisineList(
                sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                ), object : ApiListener {
                    override fun success(successData: Any) {
                        cuisineModel.value = successData as CuisineModel
                    }

                    override fun fail(failData: Throwable) {

                    }

                })
        )
    }

    fun createProduct(
        prductImage: MultipartBody.Part,
        params: HashMap<String, RequestBody>, @Part("addon[]") addons: ArrayList<Int>,
        @Part("addon_price[]") addon_prices: ArrayList<Int>
    ) {

        params.put(
            "store_id", RequestBody.create(
                MediaType.parse("text/plain"), "" + sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                )
            )
        )
        /*  params.put("_method", RequestBody.create(
              MediaType.parse("text/plain"),"PATCH"))*/

        getCompositeDisposable().add(
            shopRepository.updateProduct(
               product_Id.toInt(), prductImage, params, addons, addon_prices, object : ApiListener {
                    override fun success(successData: Any) {
                        commonSuccessResponse.value = successData as CommonSuccessResponse
                    }

                    override fun fail(failData: Throwable) {
                        navigator.showError(failData.message!!.toString())
                    }

                })
        )
    }


    fun updateWithouotImage(
        params: HashMap<String, RequestBody>, @Part("addon[]") addons: ArrayList<Int>,
        @Part("addon_price[]") addon_prices: ArrayList<Int>
    ) {

        params.put(
            "store_id", RequestBody.create(
                MediaType.parse("text/plain"), "" + sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                )
            )
        )
        /*  params.put("_method", RequestBody.create(
              MediaType.parse("text/plain"),"PATCH"))*/

        getCompositeDisposable().add(
            shopRepository.updateProduct(
                product_Id.toInt(), params, addons, addon_prices, object : ApiListener {
                    override fun success(successData: Any) {
                        commonSuccessResponse.value = successData as CommonSuccessResponse
                    }

                    override fun fail(failData: Throwable) {
                        navigator.showError(failData.message!!.toString())
                    }

                })
        )
    }

    fun getProductDetails(){
        getCompositeDisposable().add(
            shopRepository.getProductDetails(
                product_Id.toInt(), object : ApiListener {
                    override fun success(successData: Any) {
                        editProductReponse.value = successData as EditProductModel
                    }

                    override fun fail(failData: Throwable) {
                        navigator.showError(failData.message!!)
                    }

                })
        )
    }

    fun createProductFeature(
        prductImage: MultipartBody.Part,
        fimage: MultipartBody.Part,
        params: HashMap<String, RequestBody>,
        @Part("addon[]") addons: ArrayList<Int>,
        @Part("addon_price[]") addon_prices: ArrayList<Int>
    ) {
        params.put(
            "store_id", RequestBody.create(
                MediaType.parse("text/plain"), "" + sessionManager.sharedPreferences.getInt(
                    PreferenceKey.SHOP_ID,
                    0
                )
            )
        )
        /* params.put("_method", RequestBody.create(
             MediaType.parse("text/plain"),"PATCH"))*/
        getCompositeDisposable().add(
            shopRepository.updateProduct(
                product_Id.toInt(), prductImage, fimage, params, addons, addon_prices, object : ApiListener {
                    override fun success(successData: Any) {
                        commonSuccessResponse.value = successData as CommonSuccessResponse
                    }

                    override fun fail(failData: Throwable) {
                        navigator.showError(failData.message!!)
                    }

                })
        )
    }

}