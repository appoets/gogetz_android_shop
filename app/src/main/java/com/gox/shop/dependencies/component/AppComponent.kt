package com.gox.shop.dependencies.component

import com.gox.app.ui.changepasswordactivity.ChangePasswordActivity
import com.gox.app.ui.changepasswordactivity.ChangePasswordViewModel
import com.gox.shop.base.BaseRepository
import com.gox.shop.dependencies.modules.AppContainerModule
import com.gox.shop.dependencies.modules.ApplicationModule
import com.gox.shop.dependencies.modules.NetworkModule
import com.gox.shop.fcm.MyFirebaseMessagingService
import com.gox.shop.repository.ShopRepository
import com.gox.shop.utils.*
import com.gox.shop.views.accept.AcceptOrderFragment
import com.gox.shop.views.account.AccountFragment
import com.gox.shop.views.adapters.*
import com.gox.shop.views.dashboard.DashboardActivity
import com.gox.shop.views.dashboard.DashboardViewModel
import com.gox.shop.views.editrestaurant.EditRestaurantActivity
import com.gox.shop.views.editrestaurant.EditRestaurantViewModel
import com.gox.shop.views.edittime.EditTimeViewModel
import com.gox.shop.views.forgotPasswordActivity.ForgotPasswordActivity
import com.gox.shop.views.forgotPasswordActivity.ForgotPasswordViewModel
import com.gox.shop.views.history.historycancelled.FragmentCancelOrders
import com.gox.shop.views.history.historycancelled.FragmentCancelViewModel
import com.gox.shop.views.history.historyongoing.FragmentOngoingOrders
import com.gox.shop.views.history.historyongoing.FragmentOngoingViewModel
import com.gox.shop.views.history.historypast.FragmentPastOrders
import com.gox.shop.views.history.historypast.FragmentPastViewModel
import com.gox.shop.views.login.LoginActivity
import com.gox.shop.views.login.LoginViewModel
import com.gox.shop.views.orderdetail.OrderDetailActivity
import com.gox.shop.views.orderdetail.OrderDetailViewModel
import com.gox.shop.views.requestdetail.RequestDetailActivity
import com.gox.shop.views.requestdetail.RequestDetailViewModel
import com.gox.shop.views.orders.incoming.IncomingViewModel
import com.gox.shop.views.product.ProductMainFragment
import com.gox.shop.views.product.ProductMainViewModel
import com.gox.shop.views.product.addons.AddOnListViewModel
import com.gox.shop.views.product.addons.create.AddAddOnViewModel
import com.gox.shop.views.product.addons.edit.EditAddOnViewModel
import com.gox.shop.views.product.category.CategoriesViewModel
import com.gox.shop.views.product.category.create.CreateCategoryViewModel
import com.gox.shop.views.product.category.edit.EditCategoryViewModel
import com.gox.shop.views.product.categorySelection.CategorySelectionViewModel
import com.gox.shop.views.product.productDML.ProductListViewModel
import com.gox.shop.views.product.productDML.addonSelection.AddOnSelectionViewModel
import com.gox.shop.views.product.productDML.create.ProductCreateActivity
import com.gox.shop.views.product.productDML.create.ProductCreateViewModel
import com.gox.shop.views.product.productDML.edit.editproduct.EditProduct
import com.gox.shop.views.product.productDML.edit.editproduct.EditProductViewModel
import com.gox.shop.views.product.productDML.price.PriceSelectionActivity
import com.gox.shop.views.profile.ProfileViewModel
import com.gox.shop.views.revenue.RevenueViewModel
import com.gox.shop.views.selectcuisine.SelectCuisine
import com.gox.shop.views.selectcuisine.SelectCuisineViewModel
import com.gox.shop.views.splash.SplashActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        NetworkModule::class,
        ApplicationModule::class,
        AppContainerModule::class
    )
)
interface AppComponent {

    fun inject(commonMethods: CommanMethods)

    fun inject(dateTimeUtility: DateTimeUtility)

    fun inject(splashActivity: SplashActivity)

    fun inject(baseRepository: BaseRepository)

    fun inject(shopRepository: ShopRepository)

    fun inject(sessionManager: SessionManager)

    fun inject(imageUtils: ImageUtils)

    fun inject(constants: Constants)

    fun inject(myFirebaseMessagingService: MyFirebaseMessagingService)

    //Adapters
    fun inject(incomingOrderAdapter: IncomingOrderAdapter)

    fun inject(orderListAdapter: OrderListAdapter)

    fun inject(orderListHistoryDetailsAdapter: OrderListHistoryDetailsAdapter)

    fun inject(historyCommonAdapter: HistoryCommonAdapter)

    fun inject(categoryAdapter: CategoryAdapter)

    //Activity
    fun inject(loginActivity: LoginActivity)

    fun inject(dashboardActivity: DashboardActivity)

    fun inject(orderDetailActivity: RequestDetailActivity)

    fun inject(orderDetailActivity: OrderDetailActivity)

    fun inject(changePasswordActivity: ChangePasswordActivity)

    fun inject(editRestaurantActivity: EditRestaurantActivity)

    fun inject(selectCuisine: SelectCuisine)

    fun inject(forgotPasswordActivity: ForgotPasswordActivity)


    //ViewModel
    fun inject(loginViewModel: LoginViewModel)

    fun inject(incomingViewModel: IncomingViewModel)

    fun inject(dashboardViewModel: DashboardViewModel)

    fun inject(orderDetailViewModel: RequestDetailViewModel)

    fun inject(changePasswordViewModel: ChangePasswordViewModel)

    fun inject(editTimeViewModel: EditTimeViewModel)

    fun inject(fragmentOngoingViewModel: FragmentOngoingViewModel)

    fun inject(fragmentPastViewModel: FragmentPastViewModel)

    fun inject(fragmentCancelViewModel: FragmentCancelViewModel)

    fun inject(profileViewModel: ProfileViewModel)

    fun inject(editRestaurantViewModel: EditRestaurantViewModel)

    fun inject(selectCuisineViewModel: SelectCuisineViewModel)

    fun inject(viewModel: RevenueViewModel)

    fun inject(viewModel: OrderDetailViewModel)

    fun inject(forgotPasswordViewModel: ForgotPasswordViewModel)


    //Fragment
    fun inject(acceptOrderFragment: AcceptOrderFragment)

    fun inject(fragmentPastOrders: FragmentPastOrders)

    fun inject(frgmentOngoingOrders: FragmentOngoingOrders)

    fun inject(fragmentCancelOrders: FragmentCancelOrders)
    fun inject(productMainViewModel: ProductMainViewModel)
    fun inject(addOnListViewModel: AddOnListViewModel)
    fun inject(addAddOnViewModel: AddAddOnViewModel)
    fun inject(editAddOnViewModel: EditAddOnViewModel)
    fun inject(categoriesViewModel: CategoriesViewModel)
    fun inject(createCategoryViewModel: CreateCategoryViewModel)
    fun inject(editCategoryViewModel: EditCategoryViewModel)
    fun inject(productListViewModel: ProductListViewModel)
    fun inject(productCreateViewModel: ProductCreateViewModel)
    fun inject(categorySelectionViewModel: CategorySelectionViewModel)
    fun inject(addOnSelectionViewModel: AddOnSelectionViewModel)
    fun inject(accountFragment: AccountFragment)
    fun inject(productMainFragment: ProductMainFragment)
    fun inject(productCreateActivity: ProductCreateActivity)
    fun inject(priceSelectionActivity: PriceSelectionActivity)
    fun inject(editProductViewModel: EditProductViewModel)
    fun inject(editProduct: EditProduct)
    fun inject(addOnAdapter: AddOnAdapter)
    fun inject(productListAdapter: ProductListAdapter)
}